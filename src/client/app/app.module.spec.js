(function () {
  'use strict';
  // jshint -W030

  describe('Verify modules are loading', function () {

    var module;
    var dependencies = [];

    var hasModule = function (module) {
      return dependencies.indexOf(module) >= 0;
    };

    beforeEach(function () {

      // Get module
      module = angular.module('app');
      dependencies = module.requires;
    });

    it('should load app.core module', function () {
      expect(hasModule('app.core')).to.be.ok;
    });

    it('should load app.widgets module', function () {
      expect(hasModule('app.widgets')).to.be.ok;
    });

    it('should load app.customers module', function () {
      expect(hasModule('app.customers')).to.be.ok;
    });

    it('should load app.dashboard module', function () {
      expect(hasModule('app.dashboard')).to.be.ok;
    });

    it('should load app.layout module', function () {
      expect(hasModule('app.layout')).to.be.ok;
    });

    it('should load wthAngularLibSeed module', function () {
      expect(hasModule('wthAngularLibSeed')).to.be.ok;
    });

  });
}());
