# Server Integration Tests

Here we implement tests that ensure our data (see: ../../../server/data) is consistent with our expectations. This 
helps minimize issue encountered when switching between external data sources and the development data served by our
server component.
