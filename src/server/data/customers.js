'use strict';
// jshint -W065
// jshint node:true

var jsonfileservice = require(__dirname + '/../routes/utils/jsonfileservice')();
var config = require(__dirname + '/../routes/config');

module.exports = {
  getCustomer: getCustomer,
  getCustomers: getCustomers
};

function getCustomer(req, res, next) {
  var json = jsonfileservice.getJsonFromFile(qualifyFileName('customers.json'));
  var customer = json.filter(function (c) {
    return c.id === parseInt(req.params.id);
  });
  res.send(customer[0]);
}

function getCustomers(req, res, next) {
  var json = jsonfileservice.getJsonFromFile(qualifyFileName('customers.json'));
  res.send(json);
}

function qualifyFileName(file) {
  return __dirname + '/' + file;
}
