'use strict';
// jshint node: true

var config = require('../config');

module.exports = function (path) {
  return config.apiBase + path;
};
