'use strict';
// jshint node: true

module.exports = function (app) {
  var uri = require(__dirname + '/utils/apiUriBuilder');
  var customerDataService = require(__dirname + '/../data/customers');

  app.get(uri('/customer/:id'), customerDataService.getCustomer);
  app.get(uri('/customers'), customerDataService.getCustomers);
};
