# wth-angular-seed

An AngularJS seed application with a powerful `gulp` build that will provide the full project lifecycle automation you
need to be productive in your next AngularJS application. You will be able automate testing, code analysis, and release
management. Additionally, your development process will be supercharged with live browser syncing (see the affect of
your coding instantaneously in the browser) and a node served local data service that is simple to hook into.

## Requirements

- Install node
- Install (global) node dependencies

### Install node

#### OSX

Install [home brew](http://brew.sh/) and use it to install node as shown below:
 
    $ brew install node
    
NOTE (highly recommended): you can alleviate the need to run as sudo by [following these instructions](http://jpapa.me/nomoresudo).

#### Windows

Install [chocolatey](https://chocolatey.org/) and use it to install node. You will need to open a command prompt as
`administrator` and execute the commands as shown below:

    $ choco install nodejs
    $ choco install nodejs.install
    
#### Linux

Use [nvm](https://github.com/creationix/nvm). This is also an option for the other platforms but I don't have the 
experience on those platforms to be able to recommend or not.

### Install (global) node dependencies

Open a terminal/command line and invoke `npm install -g` for the required global dependencies, as shown below:

    $ npm install -g node-inspector jscs jshint bower gulp

## Quick Start Development

    $ npm install
    $ gulp
    
### Depending on a local component you are developing
 
This project includes a dependency on `wth-angular-lib-seed` in order to demonstrate how to work with a component library
project under development with your application. It is declared as a normal bower dependency (see `bower.json`) which 
will be resolved normally during a normal build. Howerver, you can switch the dependency over to a live dev dependency 
with bower's `link` API. The process is two steps: first establish the link to `wth-angular-lib-seed` and then use it. 
The steps are detailed below, but you are encouraged to refer to [bower API documentation](http://bower.io/docs/api/#link) 
to get a more complete picture.

As the build of your component library project updates the `main` artifacts they will be automatically available in 
your application with a simple refresh of the browser.
 
#### Step 1: create the link
 
 From the root of the `wth-angular-lib-seed` project execute the following bower command:

    $ bower link
    
#### Step 2: use the link
 
 From this project root execute the following bower command:

    $ bower link wth-angular-lib-seed

## Dev Tools

### JSHint

We use JSHint for static code analysis. Configuration is found in `.jshintrc`. 
For configuration options see: [http://jshint.com/docs/options]()
 
### JSCS

We use JSCS for code verification. Configuration is found in `.jscsrc`. For
style rules see: [http://jscs.info/rules]()

### Editorconfig

We use Editorconfig to enforce editor configuration settings. Like indent style 
and size, charset, and other settings like trailing whitespace and final new 
line. Configuration is found in `.editorconfig`. For more information see: 
[http://editorconfig.org/]()

## Configure IDE

### webstorm

#### Editorconfig

Webstorm 10 comes with an Editorconfig plugin that will automatically look for a `.editorcongig` file in your
project. See plugin documentation for more details. Be sure to always tell webstorm to read configuration from
 your local file.

#### JSHint

Use "File | Settings | Languages and Frameworks | JavaScript | Code Quality Tools | JSHint" to enable JSHint
and configure it.

See jetbrains help for configuration options: https://www.jetbrains.com/webstorm/help/jshint.html

IMPORTANT: "Use config files"! Be sure to check this configuration option 
on the JSHint settings page. It should be next to the "Enable" checkbox.

Be sure to configure it to look for the local `.jshintrc` file.

NOTE, consider using "File | Default Settings | Languages and Frameworks | JavaScript | Code Quality Tools | JSHint"
to apply the JSHint "Use config files" configuration as the DEFAULT for webstorm.

#### JSCS

Use "File | Settings | Languages and Frameworks | JavaScript | Code Quality Tools | JSCS" to enable JSHint
and configure it. You can also apply this configuration as a DEFAULT via "File | Default Setting | ...".

See jetbrains help for configuration options: https://www.jetbrains.com/webstorm/help/jscs.html

Be sure to configure it to look for the local `.jscsrc` file.

## Credits

Much of the content of this project was shamelessly copied from John Papa's pluralsight course on JavasSript build
automation with Gulp. I have certainly put my own spin on it, but much of the work is his, and it's great!
