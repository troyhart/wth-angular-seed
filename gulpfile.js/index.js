'use strict';
// jshint node:true

/*
 gulpfile.js
 ===========
 Rather than manage one giant configuration file responsible
 for creating multiple tasks, each task has been broken out into
 its own file in gulpfile.js/tasks. Any files in that directory get
 automatically required below.

 To add a new task, simply create a new javascript file in `tasks`
 directory. See existing tasks for examples of what you can do. If
 you have a simple/basic task you can define it in `tasks/basic.js`.
 This is where the default task is defined. Any task you define here
 should be truly basic, essential and simple.
 */

var requireDir = require('require-dir');

// Require all tasks in gulpfile.js/tasks, including subfolders
requireDir('./tasks', {recurse: true});
