'use strict';
// jshint node: true

var log = require('../lib/log');

var del = require('del');
var args = require('yargs').argv;

module.exports = clean;

///////////////////////

/**
 * A function that helps keep the filesystem clean by deleting all the given pathSelectors.
 *
 * @param {string[]} pathsSelectors - an array of paths.
 * @param {Function} done - a callback to be executed when the paths have all been deleted.
 */
function clean(pathsSelectors, done) {
  del(pathsSelectors).then(function (paths) {
    if (args.verbose) {
      log({clean: 'deleting -> ' + paths});
    }
    // execute the callback because we are done!
    done();
  });
}
