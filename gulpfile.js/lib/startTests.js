'use strict';
//jshint node:true

var log = require(__dirname + '/../lib/log');
var config = require(__dirname + '/../config')();

var fork = require('child_process').fork;
var karma = require('karma').server;

var args = require('yargs').argv;

module.exports = startTests;

////////////////////

function startTests(singleRun, done) {
  var child;
  var excludeFiles = [];
  var serverSpecs = config.serverIntegrationSpecs;

  if (args.startServers) { // gulp test --startServers
    log('Starting server');
    var savedEnv = process.env;
    savedEnv.NODE_ENV = 'dev';
    savedEnv.PORT = 8888;
    child = fork(config.nodeServer);
  } else {
    if (serverSpecs && serverSpecs.length) {
      excludeFiles = serverSpecs;
    }
  }

  karma.start({
    configFile: __dirname + '/../../karma.conf.js',
    exclude: excludeFiles,
    singleRun: !!singleRun
  }, karmaCompleted);

  function karmaCompleted(karmaResult) {
    log('Karma completed!');
    if (child) {
      log('Shutting down the child process');
      child.kill();
    }
    if (karmaResult === 1) {
      done('karma: tests failed with code ' + karmaResult);
    } else {
      done();
    }
  }
}
