'use strict';
//jshint node:true

var _ = require('lodash');
var path = require('path');
var notifier = require('node-notifier');

module.exports = notify;

////////////////////

function notify(options) {
  var gulpImage = path.join(__dirname, '/../gulp.png');
  var notifyOptions = {
    sound: 'Bottle',
    contentImage: gulpImage,
    icon: gulpImage
  };
  _.assign(notifyOptions, options);
  notifier.notify(notifyOptions);
}
