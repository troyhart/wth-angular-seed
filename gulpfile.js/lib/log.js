'use strict';
//jshint node:true

var $ = require('gulp-load-plugins')({lazy: true});

module.exports = log;

////////////////////

/**
 * A function that helps log simple (string) or complex (object) messages. Each property of a given object is logged
 * discretely with the property name collored red and the property value colored blue. Simple strings are logged in
 * blue.
 *
 * @param {object|string} message - either a string or an object.
 */
function log(message) {
  if (typeof (message) === 'object') {
    for (var item in message) {
      if (message.hasOwnProperty(item)) {
        $.util.log($.util.colors.red(item, ':'), $.util.colors.blue(message[item]));
      }
    }
  } else {
    $.util.log($.util.colors.blue(message));
  }
}
