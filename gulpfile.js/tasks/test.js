'use strict';
// jshint node:true

var startTests = require(__dirname + '/../lib/startTests');

var gulp = require('gulp');

gulp.task('test', ['vet', 'templatecache'], function (done) {
  startTests(true /* singleRun */, done);
});

gulp.task('autotest', ['vet', 'templatecache'], function (done) {
  startTests(false /* singleRun */, done);
});
