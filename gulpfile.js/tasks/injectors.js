'use strict';
// jshint node:true

var log = require(__dirname + '/../lib/log');
var config = require(__dirname + '/../config')();

var gulp = require('gulp');
var $ = require('gulp-load-plugins')({lazy: true});

gulp.task('wiredep', function () {
  log('Wire up the bower css js and our app js into the html');
  var options = config.getWiredepDefaultOptions();
  var wiredep = require('wiredep').stream;

  return gulp
    .src(config.index)
    .pipe(wiredep(options))
    .pipe($.inject(gulp.src(config.js)))
    .pipe(gulp.dest(config.client));
});

gulp.task('inject', ['wiredep', 'styles', 'templatecache'], function () {
  log('Wire up the app css into the html, and call wiredep ');

  return gulp
    .src(config.index)
    .pipe($.inject(gulp.src(config.css)))
    .pipe(gulp.dest(config.client));
});
