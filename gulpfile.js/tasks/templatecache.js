'use strict';
// jshint node:true

var log = require(__dirname + '/../lib/log');
var clean = require(__dirname + '/../lib/clean');
var config = require(__dirname + '/../config')();

var gulp = require('gulp');
var $ = require('gulp-load-plugins')({lazy: true});

gulp.task('templatecache', ['clean-code'], function () {
  log('Creating AngularJS $templateCache');

  return gulp
    .src(config.htmltemplates)
    .pipe($.minifyHtml({empty: true}))
    .pipe($.angularTemplatecache(
      config.templateCache.file,
      config.templateCache.options
    ))
    .pipe(gulp.dest(config.temp));
});

// TODO: fix me!
// I don't think this name (`clean-code`) or the implementation is appropriate for the context of its usage.
// Why delete all javascript and html from the build directory? If it is important for these to be deleted
// in the course of the build it should be reworked to be explicit where it is needed rather than happening
// by side affect...
gulp.task('clean-code', function (done) {
  var files = [].concat(
    config.temp + '**/*.js',
    config.build + '**/*.html',
    config.build + 'js/**/*.js'
  );
  clean(files, done);
});
