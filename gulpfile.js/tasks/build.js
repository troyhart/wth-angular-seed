'use strict';
// jshint node:true

var log = require(__dirname + '/../lib/log');
var notify = require(__dirname + '/../lib/notify');
var config = require(__dirname + '/../config')();

var gulp = require('gulp');
var $ = require('gulp-load-plugins')({lazy: true});
var args = require('yargs').argv;
var del = require('del');

gulp.task('build', ['optimize', 'images', 'fonts'], function () {
  var msg = {
    title: 'gulp build',
    subtitle: 'Deployed to the build folder',
    message: 'Running `gulp build`'
  };
  del(config.temp);
  log(msg.subtitle);
  notify(msg);
});

gulp.task('build-specs', ['templatecache'], function () {
  log('building the spec runner');

  var wiredep = require('wiredep').stream;
  var options = config.getWiredepDefaultOptions();
  var specs = config.specs;

  options.devDependencies = true;

  if (args.startServers) {
    specs = [].concat(specs, config.serverIntegrationSpecs);
  }

  return gulp
    .src(config.specRunner)
    .pipe(wiredep(options))
    .pipe($.inject(gulp.src(config.testlibraries),
      {name: 'inject:testlibraries', read: false}))
    .pipe($.inject(gulp.src(config.js)))
    .pipe($.inject(gulp.src(config.specHelpers),
      {name: 'inject:spechelpers', read: false}))
    .pipe($.inject(gulp.src(specs),
      {name: 'inject:specs', read: false}))
    .pipe($.inject(gulp.src(config.temp + config.templateCache.file),
      {name: 'inject:templates', read: false}))
    .pipe(gulp.dest(config.client));
});
