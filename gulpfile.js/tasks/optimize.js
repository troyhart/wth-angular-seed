'use strict';
// jshint node:true

var log = require(__dirname + '/../lib/log');
var config = require(__dirname + '/../config')();

var gulp = require('gulp');
var $ = require('gulp-load-plugins')({lazy: true});

gulp.task('optimize', ['inject', 'test'], function () {
  log('Optimizing the javascript, css, html');

  var assets = $.useref.assets({searchPath: './'});
  var templateCache = config.temp + config.templateCache.file;
  var cssFilter = $.filter('**/*.css');
  var jsLibFilter = $.filter('**/' + config.optimized.lib);
  var jsAppFilter = $.filter('**/' + config.optimized.app);

  return gulp
    .src(config.index)
    .pipe($.plumber())
    .pipe($.inject(
      gulp.src(templateCache, {read: false}), {
        starttag: '<!-- inject:templates:js -->'
      }))
    .pipe(assets)
    .pipe(cssFilter)
    .pipe($.csso())
    .pipe(cssFilter.restore())
    .pipe(jsLibFilter)
    .pipe($.uglify())
    .pipe(jsLibFilter.restore())
    .pipe(jsAppFilter)
    .pipe($.ngAnnotate())
    .pipe($.uglify())
    .pipe(jsAppFilter.restore())
    .pipe($.rev())
    .pipe(assets.restore())
    .pipe($.useref())
    .pipe($.revReplace())
    .pipe(gulp.dest(config.build))
    .pipe($.rev.manifest())
    .pipe(gulp.dest(config.build));
});
