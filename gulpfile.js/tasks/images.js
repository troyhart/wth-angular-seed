'use strict';
// jshint node:true

var log = require(__dirname + '/../lib/log');
var clean = require(__dirname + '/../lib/clean');
var config = require(__dirname + '/../config')();

var gulp = require('gulp');
var $ = require('gulp-load-plugins')({lazy: true});

gulp.task('images', ['clean-images'], function () {
  log('Copying and compressing the images');

  return gulp
    .src(config.images)
    .pipe($.imagemin({optimizationLevel: 4}))
    .pipe(gulp.dest(config.build + 'images'));
});

gulp.task('clean-images', function (done) {
  clean(config.build + 'images/**/*.*', done);
});
