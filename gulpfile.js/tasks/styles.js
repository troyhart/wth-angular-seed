'use strict';
// jshint node:true

var log = require(__dirname + '/../lib/log');
var clean = require(__dirname + '/../lib/clean');
var config = require(__dirname + '/../config')();

var gulp = require('gulp');
var $ = require('gulp-load-plugins')({lazy: true});

gulp.task('styles', ['clean-styles'], function () {
  log('Compiling Less --> CSS');

  return gulp
    .src(config.less)
    .pipe($.plumber())
    .pipe($.less())
    .pipe($.autoprefixer({browsers: ['last 2 version', '> 5%']}))
    .pipe(gulp.dest(config.temp));
});

gulp.task('clean-styles', function (done) {
  clean(config.temp + '**/*.css', done);
});
