'use strict';
// jshint node:true

var log = require(__dirname + '/../lib/log');
var config = require(__dirname + '/../config')();

var gulp = require('gulp');
var $ = require('gulp-load-plugins')({lazy: true});
var args = require('yargs').argv;

gulp.task('vet', function () {
  log('Analyzing source with JSHint and JSCS');

  return gulp
    .src(config.alljs)
    // jshint -W024
    .pipe($.if(args.verbose, $.print()))
    // jshint +W024
    .pipe($.jscs())
    .pipe($.jshint())
    .pipe($.jshint.reporter('jshint-stylish', {verbose: true}))
    .pipe($.jshint.reporter('fail'));
});
