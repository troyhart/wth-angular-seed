'use strict';
// jshint node:true

var log = require(__dirname + '/../lib/log');
var clean = require(__dirname + '/../lib/clean');
var config = require(__dirname + '/../config')();

var gulp = require('gulp');

gulp.task('clean', function (done) {
  log('Clean all.');
  var delconfig = [].concat(config.build, config.temp);
  clean(delconfig, done);
});
