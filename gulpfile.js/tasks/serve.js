'use strict';
// jshint node:true

var log = require(__dirname + '/../lib/log');
var serve = require(__dirname + '/../lib/serve');

var gulp = require('gulp');

gulp.task('serve-build', ['build'], function () {
  serve(false /* isDev */);
});

gulp.task('serve-dev', ['inject'], function () {
  serve(true /* isDev */);
});

gulp.task('serve-specs', ['build-specs'], function (done) {
  log('run the spec runner');
  serve(true /* isDev */, true /* specRunner */);
  done();
});
