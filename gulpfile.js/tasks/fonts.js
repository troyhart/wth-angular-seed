'use strict';
// jshint node:true

var log = require(__dirname + '/../lib/log');
var clean = require(__dirname + '/../lib/clean');
var config = require(__dirname + '/../config')();

var gulp = require('gulp');

gulp.task('fonts', ['clean-fonts'], function () {
  log('Copying fonts');

  return gulp
    .src(config.fonts)
    .pipe(gulp.dest(config.build + 'fonts'));
});

gulp.task('clean-fonts', function (done) {
  clean(config.build + 'fonts/**/*.*', done);
});
