'use strict';
// jshint node:true

module.exports = function () {
  var path = require('path');
  var root = path.join(__dirname, '../');
  var client = path.join(root, 'src/client/');
  var clientApp = client + 'app/';
  var report = path.join(root, 'build_report/');
  var server = path.join(root, 'src/server/');
  var specRunnerFile = 'specs.html';
  var temp = path.join(root, '.tmp/');

  var bowerrc = requireJSON(path.join(root, '.bowerrc'));

  var config = {
    /**
     * Files paths
     */
    alljs: [
      // all javascript source code
      path.join(root, 'src/**/*.js'),
      // all javascript files in the project root
      path.join(root, '*.js'),
      // all gulp build javascript source
      path.join(__dirname, '**/*.js')
    ],
    build: path.join(root, 'build/'),
    client: client,
    css: temp + '**/*.css',
    fonts: path.join(root, 'bower_components/font-awesome/fonts/**/*.*'),
    htmltemplates: clientApp + '**/*.html',
    images: client + 'images/**/*.*',
    index: client + 'index.html',
    js: [
      clientApp + '**/*.module.js',
      clientApp + '**/*.js',
      '!' + clientApp + '**/*.spec.js'
    ],
    less: client + '**/*.less',
    report: report,
    root: root,
    server: server,
    temp: temp,

    /**
     * optimized files
     */
    optimized: {
      app: 'app.js',
      lib: 'lib.js'
    },

    /**
     * template cache
     */
    templateCache: {
      file: 'templates.js',
      options: {
        module: 'app.core',
        standalone: false,
        root: 'app/'
      }
    },

    /**
     * browser sync
     */
    browserReloadDelay: 1000,

    /**
     * Bower and NPM locations
     */
    bower: {
      json: require(path.join(root, 'bower.json')),
      directory: path.join(root, bowerrc.directory),
      ignorePath: '../..'
    },
    packages: [
      path.join(root, 'package.json'),
      path.join(root, 'bower.json')
    ],

    /**
     * specs.html, our HTML spec runner
     */
    specRunner: client + specRunnerFile,
    specRunnerFile: specRunnerFile,
    testlibraries: [
      path.join(root, 'node_modules/mocha/mocha.js'),
      path.join(root, 'node_modules/chai/chai.js'),
      path.join(root, 'node_modules/mocha-clean/index.js'),
      path.join(root, 'node_modules/sinon-chai/lib/sinon-chai.js')
    ],
    specs: [clientApp + '**/*.spec.js'],

    /**
     * Karma and testing settings
     */
    specHelpers: [client + 'test-helpers/*.js'],
    serverIntegrationSpecs: [client + 'tests/server-integration/**/*.spec.js'],

    /**
     * Node settings
     */
    defaultPort: 7203,
    nodeServer: path.join(root, 'src/server/app.js')
  };

  // It's important that we are assigning the function here, not the output of the function.
  // We need this because some access to the options modify what is returned adn we don't want
  // the modifications in one usage bleeding into another usage!!!
  config.getWiredepDefaultOptions = getWiredepDefaultOptions;

  // Unlike the wiredep options, the karma configuration options are not modified, so here we
  // are assigning the output of the getKarmaOptions() function.
  config.karma = getKarmaOptions();

  return config;

  ////////////////

  function getWiredepDefaultOptions() {
    return {
      bowerJson: config.bower.json,
      directory: config.bower.directory,
      ignorePath: config.bower.ignorePath
    };
  }

  function getKarmaOptions() {
    var wiredep = require('wiredep');
    var bowerFiles = wiredep({
      directory: config.bower.directory,
      bowerJson: config.bower.json,
      dependencies: true,
      devDependencies: true
    })['js'];

    var options = {
      files: [].concat(
        bowerFiles,
        config.specHelpers,
        client + '**/*.module.js',
        client + '**/*.js',
        temp + config.templateCache.file,
        config.serverIntegrationSpecs
      ),
      exclude: [],
      coverage: {
        dir: report + 'coverage',
        reporters: [
          {type: 'html', subdir: 'report-html'},
          /*{type: 'lcov', subdir: 'report-lcov'},  I CAN'T FIGURE OUT WHY I WOULD NEED BOTH html and lcov -- their content is identical! */
          {type: 'text-summary'}
        ]
      },
      preprocessors: {}
    };
    options.preprocessors[clientApp + '**/!(*.spec)+(.js)'] = ['coverage'];
    return options;
  }

  function requireJSON(filePath) {
    var fs = require('fs');
    return JSON.parse(fs.readFileSync(filePath, 'utf8'));
  }
};
